import clr

clr.AddReference("System.Runtime.InteropServices")

from System.Text import StringBuilder
from System import Array,Double,Boolean,UInt32
import System.Runtime.InteropServices

import Thorlabs.TLDFM_64.Interop
import Thorlabs.TLDFM_64.Interop.NativeMethods

import numpy as np
import time


class ThorlabsDM:

    def __init__(self):
        _,count=Thorlabs.TLDFM_64.Interop.TLDFM.get_device_count(0)
        
        if count == 0:
            raise RuntimeError('No deformable mirror found')
            
        resourceNames=[]
        for i in range(count):
            manufacturer=StringBuilder()
            instrName=StringBuilder()
            serialNumber=StringBuilder()
            resourceName=StringBuilder()

            devAvail = Thorlabs.TLDFM_64.Interop.TLDFM.get_device_information(0, manufacturer,
                                    instrName, serialNumber, False, resourceName)[1]
            
            print(f"Found device: '{instrName.ToString()}' serial: {serialNumber.ToString()}")
            resourceNames.append(resourceName.ToString())

#        resourcelist=rm.FindRsrc(Thorlabs.TLDFM_64.Interop.TLDFM.FindPattern)
        resourceName = resourceNames[0]
        self.dm=Thorlabs.TLDFM_64.Interop.TLDFM(resourceName,True,True)

        self.dm.reset()
        self.dm.enable_hysteresis_compensation(Thorlabs.TLDFM_64.Interop.DevicePart.Both,True)
        
        self.numseg = self.dm.get_segment_count(0)[1]
        self.numtilt = self.dm.get_tilt_count(0)[1]
        
        print(f"DM has {self.numseg} segments and {self.numtilt} tilt channels")

        offsetact=Array[Double]([100.0]*self.numseg)
        offsettiptilt=Array[Double]([100.0]*self.numtilt)

        self.dm.set_voltages(offsetact,offsettiptilt)

        
    def setvoltages(self,seg,tilt):
        
        seg= np.ascontiguousarray(seg)
        assert len(seg) == self.numseg
        assert len(tilt) == self.numtilt
        
        voltactarray=Array[Double](seg)
        volttiptiltarray=Array[Double](tilt)
        self.dm.set_voltages(voltactarray,volttiptiltarray)


    def relax(self):
        """ Function to shut off the mirror.
        """
        for i in range(1000):
            volt=100.0*np.ones(43)*np.exp(-float(i)/200.0)*\
                                        np.sin(2*np.pi*float(i)/50.0)

            self.setvoltages(volt)
            time.sleep(0.002)


    def getvoltages(self):
        voltact=Array[Double]([0.0]*40)

        volttiptilt=Array[Double]([0.0]*3)

        self.dm.get_voltages(voltact,volttiptilt) # function to read voltages.

        voltages=np.zeros(43)

        for i in range(40):
            voltages[i]=voltact[i]

        for i in range(3):
            voltages[40+i]=volttiptilt[i]

        return voltages


    def initialize(self, verbose='n'):

        time.sleep(1)
        if verbose=='y':
            print(f"\nMirror init:\n{self.getvoltages()}")
        #relazation
        self.relax()
        time.sleep(1)
        if verbose=='y':
            print(f"\nMirror relax:\n{self.getvoltages()}")
        self.setvoltages(np.zeros((43)))
        time.sleep(1)
        if verbose=='y':
            print(f"\nMirror zero:\n{ self.getvoltages()}")

        return None
        
    def __enter__(self):
        return self
    
    def close(self):
        if self.dm is not None:
            self.dm.Dispose()
            self.dm = None
        

    def __exit__(self, *args):
        self.close()
        
    
if __name__=="__main__":
    with ThorlabsDM() as dm:
        print(dm.getvoltages())
        
    #thor.setvoltages(np.ones(43)*0.0)
