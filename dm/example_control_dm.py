import dmctr # class for DM control
import pickle
from time import sleep
#----------------------Mirror init----------------------------------------------
dm = dmctr.dm()
dm.initialize('n')

#----------------------Mirror to base voltages --------------------------------- 
base_voltages = pickle.load(open("optim_voltages.pickle", "rb"))   
dm.setvoltages(base_voltages)
print(f"base_voltages {dm.getvoltages()}") 
sleep(.1)
