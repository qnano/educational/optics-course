import clr

clr.AddReference("System.Runtime.InteropServices")
import sys

sys.path.append('C:\Program Files (x86)\Thorlabs\Deformable Mirror')

sys.path.append('C:\Program Files (x86)\IVI Foundation\VISA\VisaCom\Primary_Interop_Assemblies')

sys.path.append('C:\Python_scripts\pozziSLMv2')
from System import Array,Double,Boolean
import System.Runtime.InteropServices

import Thorlabs.TLDFM_64.Interop
import Thorlabs.TLDFM_64.Interop.NativeMethods
import Ivi.Visa.Interop

import numpy as np
import random as rnd
import time
import pickle
from PIL import Image

import my_math_functions as mtm
reload(mtm)

class dm:

    def __init__(self):
        print ("asd")
        rm=Ivi.Visa.Interop.ResourceManager()

        resourcelist=rm.FindRsrc(Thorlabs.TLDFM_64.Interop.TLDFM.FindPattern)

        self.dm=Thorlabs.TLDFM_64.Interop.TLDFM(resourcelist[0],True,True)

        self.dm.reset()

        self.dm.enable_hysteresis_compensation(Thorlabs.TLDFM_64.Interop.DMPTarget.Both,True)


        offsetact=Array[Double]([100.0]*40)

        offsettiptilt=Array[Double]([100.0]*3)

        self.dm.set_voltages(offsetact,offsettiptilt)


    def setvoltages(self,volt):
        """ function to add to each actuator voltage 100. If the  result is
            bigger than 200, put 200: if result less 0 put 0.
        """
        # initialization @ 100 defaulta value
        if volt.shape[0]==40:
            volt=volt+100.0
            # if negative input --> not good
            if volt[volt<0.0].size:
                print "out of bounds"
            volt[volt<0.0]=0.0
            if volt[volt>200.0].size:
                print "out of bounds"
            volt[volt>200.0]=200.0

            voltactarray=Array[Double](volt)
            volttiptiltarray=Array[Double](np.zeros(3))
            # why 43????
        elif volt.shape[0]==43:

            voltact=volt[0:40]+100.0
            if voltact[voltact>200.0].size:
                print "out of bounds"
            voltact[voltact>200.0]=200.0
            if voltact[voltact<0.0].size:
                print "out of bounds"
            voltact[voltact<0.0]=0.0

            volttiptilt=volt[40:43]+100.0
            if volttiptilt[volttiptilt>200.0].size:
                print "out of bounds"
            volttiptilt[volttiptilt>200.0]=200.0
            if volttiptilt[volttiptilt<0.0].size:
                print "out of bounds"
            volttiptilt[volttiptilt<0.0]=0.0


            voltactarray=Array[Double](voltact)
            volttiptiltarray=Array[Double](volttiptilt)
        else:
            print "volt array size not correct"

        self.dm.set_voltages(voltactarray,volttiptiltarray)


    def relax(self):
        """ Function to shut off the mirror.
        """
        for i in range(1000):
            volt=100.0*np.ones(43)*np.exp(-float(i)/200.0)*\
                                        np.sin(2*np.pi*float(i)/50.0)


            self.setvoltages(volt)

            time.sleep(0.002)






    def getvoltages(self):
        voltact=Array[Double]([0.0]*40)

        volttiptilt=Array[Double]([0.0]*3)

        self.dm.get_voltages(voltact,volttiptilt) # function to read voltages.

        voltages=np.zeros(43)

        for i in range(40):
            voltages[i]=voltact[i]

        for i in range(3):
            voltages[40+i]=volttiptilt[i]

        return voltages


    def initialize(self, verbose='n'):

        time.sleep(1)
        if verbose=='y':
            print str('\nMirror init:\n'), self.getvoltages()
        #relazation
        self.relax()
        time.sleep(1)
        if verbose=='y':
            print str('\nMirror relax:\n'), self.getvoltages()
        self.setvoltages(np.zeros((43)))
        time.sleep(1)
        if verbose=='y':
            print str('\nMirror zero:\n'), self.getvoltages()

        return None
        
        
    def mirror_flat(self, camera, save_path, iterations, average, threshold, height, width, ROI):
        """Search of DM base voltages. The fuction is based on pokink one actuator
        at time and optimize the second moment of the image.
        The function returns some variables to check that everithing worked out
        This function save in the save_path start/
        image, final image, base voltages and a readme file with the start and 
        final second moment. 
        """
    

        image = np.zeros((height, width), dtype = np.float64)

        for k in range (average):
            camera.snap(False)
            image += camera.snapped/float(average)
        image = np.asarray(image)
        original = image 

        # save and show original image
        # fig = plt.figure(' Original Image ')
        # plt.imshow(original, interpolation='none')
        # plt.show()

        im=Image.fromarray(original)
        im.save(save_path+'original.tif')    
    
        #select ROI
        start_image=original[ROI[0]:ROI[1],ROI[2]:ROI[3]]
        # fig = plt.figure('Start  image ROI')
        # plt.imshow(start_image, interpolation='none')
        # plt.show() 

        #save ROI start
        roi_start=Image.fromarray(original[ROI[0]:ROI[1],ROI[2]:ROI[3]])
        roi_start.save(save_path+'roi_start.tif')
    
#----------------------Voltage variables, min_secondmoment init-----------------
        voltages = np.zeros(43)
        voltages_max = np.zeros(43)
        min_secondmoment = mtm.secondmoment(start_image,threshold)
        start_secondmoment = min_secondmoment
        # print str('secondmoment start'), min_secondmoment
        # t0=clock()
#----------------Some checks----------------------------------------------------
        minimi=[] # all the second moment measured during the loop
        better = [] # all the optimized images 
        volt_buoni = [] # all voltages corresponding to the optimized images 
        volt_buoni_set = []# all the set voltages corresponding to the optimized images 
#----------------------Loop-----------------------------------------------------
        camera.mmc.startContinuousSequenceAcquisition(0)
        for i in range(iterations):

            voltages = np.copy(voltages_max)
            actuator = rnd.randint(0,39)

    
            step = 10*(rnd.randint(0,1)-0.5) # step smaller that 5 volts 
    

            voltages[actuator] = voltages[actuator]+step
            self.setvoltages(voltages)
            # self.relax()
            time.sleep(.5) # I changed this sleep from 2 to .5
    
            temp_image = np.zeros((height, width), dtype = np.float64)
            for k in range (average):
                camera.mmc.clearCircularBuffer()
                while camera.mmc.getBufferTotalCapacity()-camera.mmc.getBufferFreeCapacity()==0:
                    pass
                camera.mmc.clearCircularBuffer()
                while camera.mmc.getBufferTotalCapacity()-camera.mmc.getBufferFreeCapacity()==0:
                    pass
            
                temp_image += camera.mmc.popNextImage()/float(average)
            temp_image=np.asarray(temp_image)
            better.append(temp_image)
            temp_image = temp_image[ROI[0]:ROI[1],ROI[2]:ROI[3]]
       
            min_secondmoment_new = mtm.secondmoment(temp_image,threshold)
            minimi.append(min_secondmoment_new)
            # print str('for'), i, self.getvoltages(), min_secondmoment_new

            #save in valtages max the optimized voltage (corresponds to reduced psf)
            if(min_secondmoment_new < min_secondmoment):
                min_secondmoment = min_secondmoment_new
                voltages_max = np.copy(voltages)
                # print  str('cond 1'),  i,  min_secondmoment, voltages_max
                # better.append(temp_image)
                volt_buoni.append(voltages_max)
                volt_buoni_set.append(self.getvoltages())
        
            
        
        #back to old voltages (since the new one corresponds to increased psf )
        else:
            self.setvoltages(voltages_max)
            # self.relax()
            time.sleep(.5)
        camera.mmc.stopSequenceAcquisition()

#--------------------------------Save base voltages-----------------------------

        pickle.dump(voltages_max, open(save_path+"optim_voltages.p", "wb"))

#--------------------------------Acquire and save final image-------------------
        
        index_min=np.argmax(minimi)
        im_base_volt=better[index_min]
        im = Image.fromarray(im_base_volt)
        im.save(save_path+'im_base_volt.tif')

        for k in range (average):
        
            camera.snap(False)
            image += camera.snapped/float(average)
        final = np.asarray(image)
    
        im = Image.fromarray(final)
        im.save(save_path+'final.tif')
        ##Show image
        # fig = plt.figure(' Final Image ')
        # plt.imshow(final,interpolation='none')
        # plt.show()
 
        #some checks 
        final_secondmoment =mtm.secondmoment(final[ROI[0]:ROI[1],ROI[2]:ROI[3]],threshold)
        # print str('final second_moment'), final_secondmoment
        #final_volt = self.getvoltages()
        # print str('final volt'), final_volt
        #save ROI final
        roi_final=Image.fromarray(final[ROI[0]:ROI[1],ROI[2]:ROI[3]])
        roi_final.save(save_path+'roi_final.tif')
        ##Show ROI image
        # fig = plt.figure(' Final ROI ')
        # plt.imshow(final[ROI[0]:ROI[1],ROI[2]:ROI[3]],interpolation='none')
        # plt.show()
        real_min=np.min(minimi)
        np.save(save_path+'minimi',minimi)
        f= open(save_path+'readme.txt',"w+")
        f.write("start second moment %f\r\n" % start_secondmoment)
        f.write("final second moment %f\r\n" % final_secondmoment)
        f.write("real min second moment %f\r\n" % real_min)
        f.write("ROI %s\r\n" % ROI)
        f.close
          
        return start_secondmoment, final_secondmoment, minimi, better, volt_buoni,\
                volt_buoni_set   
        
      
        
            

if __name__=="__main__":
    thor=dm()
    thor.setvoltages(np.ones(43)*0.0)
