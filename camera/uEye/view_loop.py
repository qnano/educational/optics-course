# -*- coding: utf-8 -*-

# -*- coding: utf-8 -*-
"""
Created on Wed Apr 24 13:46:46 2019

@author: jcnossen1
"""

import pygame as pg
import matplotlib.pyplot as plt
import numpy as np
from pyueye_example_camera import Camera
from pyueye_example_utils import ImageBuffer,ImageData

from pyueye import ueye

import scipy.misc

import matplotlib.pyplot as plt

def fig2data(fig):
    fig.canvas.draw()
    return np.array(fig.canvas.renderer._renderer)


class App:

    def __init__(self):
        pg.init()
        
        self.wndsize = (800,600)
        self.screen = pg.display.set_mode(self.wndsize)
        self.mousepos = (0,0)
        
        self.exposure= 0.01

        pg.mouse.set_visible(False)
        
        # camera class to simplify uEye API access
        cam = Camera(1)
        cam.init()
        cam.set_colormode(ueye.IS_CM_BGR8_PACKED)
        cam.set_aoi(0,0, 1280, 1024)
        _, exp = cam.set_exposure(0.001)

        cam.alloc()
        cam.capture_video()
        self.cam = cam
    
    def grab(self):
        cam = self.cam
        
        timeout = 1000
    
        img_buffer = ImageBuffer()
        ret = ueye.is_WaitForNextImage(cam.handle(),
                                       timeout,
                                       img_buffer.mem_ptr,
                                       img_buffer.mem_id)
        
        imgdata = None
        if ret == ueye.IS_SUCCESS:
            imgdata = ImageData(cam.handle(), img_buffer)
            
            img = imgdata.as_1d_image()
        
            # unlock the buffer so we can use it again
            imgdata.unlock()
            return img
        
        return None

    def draw_fig(self, dstsurf, fig, pos, size=(400,400)):
        w,h=size
        
        dpi = 80
        fig.set_dpi(dpi)
        fig.set_size_inches(w/dpi,h/dpi)

        d = fig2data(fig)
        rgb = d[:,:,:3].transpose((1,0,2))
        
        x,y=int(pos[0]),int(pos[1])
        screen_array = pg.surfarray.pixels3d(dstsurf)
        screen_array[x:w+x, y:h+y] = rgb
        del screen_array


    def draw(self):
        self.screen.fill(0)

        img = self.grab()
        if img is not None:
            img = scipy.misc.imresize(img, self.wndsize)
                    
            screen_array = pg.surfarray.pixels3d(self.screen)
            screen_array[:img.shape[0],:img.shape[1]] = img
            del screen_array
        
        pg.draw.circle(self.screen, pg.Color(0,0,255), self.mousepos, 5)
        pg.display.update()
            
    def main(self):
        running = True

        # main loop
        while running:
            # event handling, gets all event from the event queue
            for event in pg.event.get():
                if event.type == pg.KEYDOWN:
                    if event.key == pg.K_ESCAPE:
                        running = False
                        
                    if event.key == pg.K_o:
                        self.exposure /=1.1
                        self.cam.set_exposure(self.exposure)
                        print(f"Exposure: {self.exposure}")

                    if event.key == pg.K_p:
                        self.exposure *= 1.1
                        self.cam.set_exposure(self.exposure)
                        print(f"Exposure: {self.exposure}")
                
                if event.type == pg.QUIT:
                    running = False
                    
                    
                if event.type == pg.MOUSEMOTION:
                    self.mousepos = event.pos
                    
            self.draw()

        self.cam.stop_video()
        self.cam.exit()
         

if __name__=="__main__":
    try:    
        # call the main function
        App().main()
    finally:         
        pg.display.quit()
        pg.quit()
        
    