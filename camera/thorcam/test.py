"""
https://pypi.org/project/Instrumental-lib/

https://instrumental-lib.readthedocs.io/en/stable/uc480-cameras.html

pip install Instrumental-lib
pip install pywin32 nicelib

""" 

from instrumental.drivers.cameras import uc480
import matplotlib.pyplot as plt


cam = uc480.UC480_Camera()

img = cam.grab_image(n_frames=1)

cam.close()

print(img.shape)

#plt.imshow(img)
