import clr

clr.AddReference("System.Runtime")
clr.AddReference("System.Runtime.InteropServices")

from System.Text import StringBuilder
from System import Array,Double,Boolean,UInt32,Int32
import System.Runtime.InteropServices

clr.AddReference('uc480DotNet')

from uc480 import Camera
import uc480.Defines

class ThorlabsCamera():
    
    def __init__(self):
        self.inst = Camera()
        status = self.inst.Init()
        if status != uc480.Defines.Status.SUCCESS:
            raise RuntimeError('Failed to init camera')
            
        status,self.memID = self.inst.Memory.Allocate(Int32(0),True)
        if status != uc480.Defines.Status.SUCCESS:
            raise RuntimeError("Failed to allocate memory")
            
        w = self.inst.Memory.GetWidth(self.memID,0)[1]
        h = self.inst.Memory.GetHeight(self.memID,0)[1]
        pitch = self.inst.Memory.GetPitch(self.memID,0)[1]
        bpp = self.inst.Memory.GetBitsPerPixel(self.memID,0)[1]
        
        print(f"Width={w}, height={h}, pitch={pitch}, bpp={bpp}")
            
    def begin(self):
        status = self.inst.Acquisition.Capture()
        if status != uc480.Defines.Status.SUCCESS:
            raise RuntimeError('Failed to start capture')

    def stop(self):
        self.inst.Acquisition.Stop()
        
    def readout(self):
        self.inst.Memory.GetActive()
    
    def __enter__(self):
        return self

    def __exit__(self, *args):
        if self.inst is not None:
            self.inst.Exit()
            self.inst=None


if __name__=="__main__":
    with ThorlabsCamera() as cam:
        ...
        
        