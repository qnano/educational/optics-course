"""
https://pypi.org/project/Instrumental-lib/

""" 

from instrumental.drivers.cameras import uc480
import pygame as pg
import pygame.freetype
import matplotlib.pyplot as plt
import numpy as np
import scipy.misc

import sys
sys.path.append("dm")
from dm.dmctr import ThorlabsDM

def fig2data(fig):
    fig.canvas.draw()
    return np.array(fig.canvas.renderer._renderer)


class App:
    def __init__(self):
        pg.init()
    
        pg.font.init()
        self.font = pg.freetype.SysFont('Comic Sans MS', 30)

        self.screen = pg.display.set_mode((600,600))
        pg.display.set_caption('Thorlabs DM with random activation')
        self.mousepos = (0,0)
        self.wndsize = (self.screen.get_width(),self.screen.get_height())

        pg.mouse.set_visible(False)
        
        self.dm = ThorlabsDM()

        self.cam = uc480.UC480_Camera()
        self.cam.start_live_video(framerate= '10 Hz')
        
    def close(self):
        self.dm.close()
        
        print('closing camera')
        self.cam.stop_live_video()
        self.cam.close()
        del self.cam

    def draw_fig(self, dstsurf, fig, pos, size=(400,400)):
        w,h=size
        
        dpi = 80
        fig.set_dpi(dpi)
        fig.set_size_inches(w/dpi,h/dpi)

        d = fig2data(fig)
        rgb = d[:,:,:3].transpose((1,0,2))
        
        x,y=int(pos[0]),int(pos[1])
        screen_array = pg.surfarray.pixels3d(dstsurf)
        screen_array[x:w+x, y:h+y] = rgb + rgb*256 + rgb*256*256
        del screen_array

    def draw_histogram(self, img):
        hist = np.histogram(img.flatten(),bins=255, range=(0,255), density=True)[0]      
        pg.draw.lines(self.screen, pg.Color(0,0,255), False, [(x,self.wndsize[1]-y*200) for x,y in enumerate(hist)], 1)

    def draw(self):
        img = self.cam.latest_frame()
        img = scipy.misc.imresize(img, self.wndsize).astype(np.uint32)
        self.last_img=img
                
        screen_array = pg.surfarray.pixels2d(self.screen)
        screen_array[:img.shape[0],:img.shape[1]] = img + (img<<8) + (img<<16)
        del screen_array
        
        self.font.render_to(self.screen, (0, self.wndsize[1]-30), "C: reset, R: random", (255,255, 255))

        self.draw_histogram(img)
        
        pg.draw.circle(self.screen, pg.Color(0,0,255), self.mousepos, 5)

        if False:
            fig = plt.figure()
            plt.hist(self.last_img.flatten(), bins=255, range=(0,255))
    #        plt.plot(np.sin(np.linspace(0,5) - self.mousepos[0] * 0.01))
            self.draw_fig(self.screen, fig, pos=(0,self.screen.get_height()-200), size=(300,200))
            plt.close(fig)

        pg.display.update()
            
    def main(self):
        running = True

        # main loop
        while running:
            # event handling, gets all event from the event queue
            for event in pg.event.get():
                if event.type == pg.KEYDOWN:
                    if event.key == pg.K_ESCAPE:
                        running = False
                        
                    if event.key == pg.K_r:
                        seg=np.random.uniform(0,200, size=self.dm.numseg)
                        tilt = np.random.uniform(0,200, size=self.dm.numtilt)
                        self.dm.setvoltages(seg,tilt)
                        print(f"setting DM: seg={seg}, tilt={tilt}")

                    if event.key == pg.K_c:
                        seg=np.ones(self.dm.numseg)*100
                        tilt = np.ones(self.dm.numtilt)*100
                        self.dm.setvoltages(seg,tilt)
                        print(f"setting DM: seg={seg}, tilt={tilt}")
                
                if event.type == pg.QUIT:
                    running = False
                    
                    
                if event.type == pg.MOUSEMOTION:
                    self.mousepos = event.pos
                    
            self.draw()
            
        self.close()
         

if __name__=="__main__":
    try:
        app = App()
        # call the main function
        app.main()
        
    finally:
        pg.display.quit()
        pg.quit()
        
    