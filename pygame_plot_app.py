# -*- coding: utf-8 -*-
"""
Created on Wed Apr 24 13:46:46 2019

@author: jcnossen1
"""

import pygame as pg
import matplotlib.pyplot as plt
import numpy as np


def fig2data(fig):
    fig.canvas.draw()
    return np.array(fig.canvas.renderer._renderer)


class App:

    def __init__(self):
        pg.init()
    
        self.screen = pg.display.set_mode((800,600))
        self.mousepos = (0,0)

        pg.mouse.set_visible(False)

    def draw_fig(self, dstsurf, fig, pos, size=(400,400)):
        w,h=size
        
        dpi = 80
        fig.set_dpi(dpi)
        fig.set_size_inches(w/dpi,h/dpi)

        d = fig2data(fig)
        rgb = d[:,:,:3].transpose((1,0,2))
        
        x,y=int(pos[0]),int(pos[1])
        screen_array = pg.surfarray.pixels3d(dstsurf)
        screen_array[x:w+x, y:h+y] = rgb
        del screen_array


    def draw(self):
        self.screen.fill(0)
        pg.draw.circle(self.screen, pg.Color(0,0,255), self.mousepos, 5)

        fig = plt.figure()
        plt.plot(np.sin(np.linspace(0,5) - self.mousepos[0] * 0.01))
        self.draw_fig(self.screen, fig, pos=(0,self.screen.get_height()-200), size=(200,200))
        plt.close(fig)

        pg.display.update()
            
    def main(self):
        running = True

        # main loop
        while running:
            # event handling, gets all event from the event queue
            for event in pg.event.get():
                if event.type == pg.KEYDOWN:
                    if event.key == pg.K_ESCAPE:
                        running = False
                
                if event.type == pg.QUIT:
                    running = False
                    
                if event.type == pg.MOUSEMOTION:
                    self.mousepos = event.pos
                    
            self.draw()

         

if __name__=="__main__":
    try:    
        # call the main function
        App().main()
    finally:         
        pg.display.quit()
        pg.quit()
        
    