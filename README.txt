Download camera and deformable mirror drivers:
http://homepage.tudelft.nl/f04a3/optics-course-drivers.zip

First install all required python packages using

pip install -r requirements.txt

Pythonnet is used for Thorlabs DM control, but does not always install well using pip, so use conda instead (https://anaconda.org/pythonnet/pythonnet):

conda install -c pythonnet pythonnet 
